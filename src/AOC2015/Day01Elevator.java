package AOC2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day01Elevator {

	public static void main(String[] args) throws IOException {
		long status = 0;
		long min = 0;
		String line = Files.readString(Paths.get("01.txt"));
		for (int i = 0; i < line.length(); i++) {
			status += switch (line.charAt(i)) {
				case '(' -> 1;
				case ')' -> -1;
				default -> 0;
			};
			if ( status == -1 && min == 0 )
				min = i + 1;
		}
		System.out.println(status + " " + min);
	}
}
