package AOC2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Day24Balance {

	static boolean inc(int[] s, int m) {
		for (int i = s.length - 1; i >= 0; i--) {
			s[i]++;
			if ( s[i] == m - (s.length - i) + 1 )
				continue;
			for (int j = i + 1; j < s.length; j++) {
				s[j] = s[j - 1] + 1;
				if ( s[j] >= m )
					return false;
			}
			break;
		}
		return s[0] != m - (s.length - 1);
	}

	public static void main(String[] args) throws IOException {
		int[] nums = Files.readAllLines(Paths.get("24.txt")).stream().mapToInt(Integer::valueOf).toArray();
		int sum = Arrays.stream(nums).sum();
		int tot = sum / 4;

		int[] s = new int[5];
		for (int i = 0; i < s.length; i++)
			s[i] = i;
		long min = 1_000_000_000_000L;
		while (inc(s, nums.length)) {
			long v = 0;
			for (int i : s)
				v += nums[i];
			if ( v != tot )
				continue;
			long m = 1;
			for (int i : s)
				m *= nums[i];
			if ( m < min )
				min = m;
		}
		System.out.println(min);
	}
}
