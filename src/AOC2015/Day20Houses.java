package AOC2015;

public class Day20Houses {

	public static void main(String[] args) {
		int lim = 34_000_000;
		for (int i = 10000; true; i++) {
			//int sum = sum1(i);
			int sum = sum2(i);
			if ( sum >= lim ) {
				System.out.println(i);
				break;
			}
		}
	}

	static int sum1(int i) {
		int sum = i + 1;
		int fin = (int)Math.sqrt(i) + 1;
		for (int j = 2; j < fin; j++) {
			if ( i % j != 0 )
				continue;
			int k = i / j;
			sum += j;
			if ( j != k )
				sum += k;
		}
		return 10 * sum;
	}

	static int sum2(int i) {
		int sum = 0;
		for (int j = 1; j <= 50; j++) {
			if ( i % j == 0 )
				sum += i / j;
		}
		return sum * 11;
	}
}
