package AOC2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Day02PresentWrap {

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		for (String s : Files.readAllLines(Paths.get("02.txt"))) {
			var list = Arrays.stream(s.split("x")).map(Integer::parseInt).sorted().toList();
			int a = list.get(0);
			int b = list.get(1);
			int c = list.get(2);
			sum1 += 2 * (a * b + a * c + b * c) + a * b;
			sum2 += a + a + b + b + a * b * c;
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
