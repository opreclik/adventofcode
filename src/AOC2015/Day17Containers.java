package AOC2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class Day17Containers {

	static void sol1(int v[]) {
		long[][] map = new long[151][v.length];
		for (int i = 1; i < map.length; i++) {
			for (int j = 0; j < v.length; j++) {
				int r = i - v[j];
				if ( j > 0 ) {
					map[i][j] = map[i][j - 1];
					if ( r > 0 )
						map[i][j] += map[r][j - 1];
				}
				if ( r == 0 )
					map[i][j]++;
			}
		}
		System.out.println(map[150][v.length - 1]);
	}

	static void sol2(int[] v) {
		int[] idx = new int[20];
		for (int m = 4; m < 20; m++) {
			for (int i = 0; i < m; i++)
				idx[i] = i;
			idx[m] = v.length;
			int all = 0;
			while (step(idx, m)) {
				if ( Arrays.stream(idx, 0, m).map(i -> v[i]).sum() == 150 )
					all++;
			}
			if ( all > 0 )
				System.out.println(m + " " + all);
		}
	}

	static boolean step(int[] idx, int m) {
		for (int i = m - 1; i >= 0; i--) {
			if ( idx[i] + 1 < idx[i + 1] ) {
				idx[i]++;
				for (int j = i + 1; j < m; j++)
					idx[j] = idx[j - 1] + 1;
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) throws IOException {
		int[] v = Files.readAllLines(Paths.get("17.txt")).stream()
			.mapToInt(Integer::parseInt).toArray();
		Arrays.sort(v, 0, v.length);

		sol1(v);
		sol2(v);
	}
}
