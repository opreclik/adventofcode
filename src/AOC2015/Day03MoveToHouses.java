package AOC2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class Day03MoveToHouses {

	static record House(int x, int y) {}

	static class Bot {

		int x;
		int y;

		void move(char c, Set<House> set) {
			switch (c) {
				case '>' -> x++;
				case '<' -> x--;
				case 'v' -> y--;
				case '^' -> y++;
			}
			set.add(new House(x, y));
		}
	};

	public static void main(String[] args) throws IOException {
		Bot a = new Bot();
		Bot x = new Bot();
		Bot y = new Bot();

		Set<House> m1 = new HashSet<>();
		Set<House> m2 = new HashSet<>();

		m1.add(new House(0, 0));
		m2.add(new House(0, 0));
		String line = Files.readString(Paths.get("03.txt"));
		for (int i = 0; i < line.length(); i++) {
			char c = line.charAt(i);
			a.move(c, m1);
			((i & 1) == 0 ? x : y).move(c, m2);
		}
		System.out.println(m1.size() + " " + m2.size());
	}
}
