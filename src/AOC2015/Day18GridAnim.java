package AOC2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day18GridAnim {

	public static void main(String[] args) throws IOException {
		int[][] a = new int[105][105];
		int[][] b = new int[105][105];
		int row = 1;
		for (String s : Files.readAllLines(Paths.get("18.txt"))) {
			for (int i = 0; i < s.length(); i++) {
				if ( s.charAt(i) == '#' )
					a[row][i + 1] = 1;
			}
			row++;
		}
		int sum = 0;
		for (int i = 0; i < 100; i++) {
			sum = 0;
			for (int r = 1; r <= 100; r++) {
				for (int c = 1; c <= 100; c++) {
					int cnt = a[r - 1][c - 1]
							  + a[r - 1][c]
							  + a[r - 1][c + 1]
							  + a[r][c - 1]
							  + a[r][c + 1]
							  + a[r + 1][c - 1]
							  + a[r + 1][c]
							  + a[r + 1][c + 1];

					int p = a[r][c];
					b[r][c] = (cnt == 3) || (p == 1 && cnt == 2) ? 1 : 0;
					if ( (r == 1 || r == 100) && (c == 1 || c == 100) )
						b[r][c] = 1;
					System.out.print(b[r][c]);
					if ( b[r][c] == 1 )
						sum++;
				}
				System.out.println("");
			}
			System.out.println("");
			int[][] t = a;
			a = b;
			b = t;
		}
		System.out.println(sum);
	}
}
