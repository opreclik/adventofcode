package AOC2015;

public class Day21RPG {

	static record Item(int gold, int wep, int arm) {};

	static Item[] wep = new Item[]{
		new Item(8, 4, 0),
		new Item(10, 5, 0),
		new Item(25, 6, 0),
		new Item(40, 7, 0),
		new Item(74, 8, 0),
	};

	static Item[] arm = new Item[]{
		new Item(0, 0, 0),
		new Item(13, 0, 1),
		new Item(31, 0, 2),
		new Item(53, 0, 3),
		new Item(75, 0, 4),
		new Item(102, 0, 5),
	};

	static Item[] rings = new Item[]{
		new Item(0, 0, 0),
		new Item(25, 1, 0),
		new Item(50, 2, 0),
		new Item(100, 3, 0),
		new Item(20, 0, 1),
		new Item(40, 0, 2),
		new Item(80, 0, 3),
	};

	static class It {

		int w;
		int a;
		int r1;
		int r2 = -1;

		boolean step() {
			r2++;
			if ( r2 == rings.length ) {
				r1++;
				r2 = r1 + 1;
				if ( r1 == rings.length - 1 ) {
					a++;
					r1 = r2 = 0;
					if ( a == arm.length ) {
						w++;
						a = 0;
						if ( w == wep.length )
							return false;
					}
				}
			}
			return true;
		}
	}

	public static void main(String[] args) {
		int g1 = 1000;
		int g2 = 0;
		for (It i = new It(); i.step();) {
			int d1 = wep[i.w].wep + rings[i.r1].wep + rings[i.r2].wep - 2;
			int d2 = Math.max(8 - (arm[i.a].arm + rings[i.r1].arm + rings[i.r2].arm), 1);
			int gold = wep[i.w].gold + arm[i.a].gold + rings[i.r1].gold + rings[i.r2].gold;
			if ( 100 > (109 / d1) * d2 )
				g1 = Math.min(gold, g1);
			else
				g2 = Math.max(gold, g2);
		}
		System.out.println(g1 + " " + g2);
	}
}
