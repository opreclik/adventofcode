package AOC2015;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day19Rules {

	static record Rule(String a, String b) {};

	public static void main(String[] args) throws IOException {
		List<Rule> rules = new ArrayList<>();
		String s = null;
		for (String a : Files.readAllLines(Paths.get("19.txt"))) {
			if ( a.isEmpty() )
				continue;
			if ( a.contains("=>") ) {
				String[] sp = a.split(" ");
				rules.add(new Rule(sp[0], sp[2]));
				continue;
			}
			s = a;
			break;
		}
		Set<String> v = new HashSet<>();
		for (Rule r : rules)
			for (int i = s.indexOf(r.a); i != -1; i = s.indexOf(r.a, i + 1))
				v.add(s.substring(0, i) + r.b + s.substring(i + r.a.length()));
		System.out.println(v.size());

		int i = 0;
		while (!s.equals("e")) {
			for (Rule r : rules) {
				if ( s.contains(r.b) ) {
					s = s.replaceFirst(r.b, r.a);
					i++;
				}
			}
		}
		System.out.println(i);
	}
}
