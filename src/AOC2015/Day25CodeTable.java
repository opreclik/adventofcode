package AOC2015;

public class Day25CodeTable {

	public static void main(String[] args) {
		long val = 20151125;
		for (int r = 2; true; r++) {
			for (int c = 1, a = r; c <= r; c++, a--) {
				val = (val * 252533) % 33554393;
				if ( a == 2981 && c == 3075 ) {
					System.out.println(val);
					return;
				}
			}
		}
	}
}
