package AOC2020;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day01Sums {

	public static void main(String[] args) throws IOException {
		int[] v = Files.readAllLines(Paths.get("01.txt")).stream().mapToInt(Integer::parseInt).toArray();
		for (int i = 0; i < v.length; i++) {
			for (int j = i + 1; j < v.length; j++) {
				for (int k = j + 1; k < v.length; k++) {
					if ( v[i] + v[j] + v[k] == 2020 ) {
						System.out.println(v[i] * v[j] * v[k]);
						break;
					}
				}
			}
		}
	}
}
