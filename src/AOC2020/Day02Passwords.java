package AOC2020;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import utils.ParseNum;

public class Day02Passwords {

	public static void main(String[] args) throws IOException {
		int r1 = 0;
		int r2 = 0;
		for (String a : Files.readAllLines(Paths.get("02.txt"))) {
			String[] s = a.split(" ");
			ParseNum pn = new ParseNum();
			int p = pn.read(s[0], 0);
			int i = pn.val;
			pn.read(s[0], p + 1);
			int j = pn.val;
			char c = s[1].charAt(0);
			long cnt = s[2].chars().filter(x -> x == c).count();
			if ( cnt >= i && cnt <= j )
				r1++;
			if ( (s[2].charAt(i - 1) == c) != (s[2].charAt(j - 1) == c) )
				r2++;
		}
		System.out.println(r1);
		System.out.println(r2);
	}
}
