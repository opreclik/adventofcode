package utils;

public class Utils {

	public static int[] dx = {1, 0, -1, 0};
	public static int[] dy = {0, 1, 0, -1};

	public static char a2d(char c) {
		return switch (c) {
			case 'R' -> 0;
			case 'D' -> 1;
			case 'L' -> 2;
			case 'U' -> 3;
			default -> throw new RuntimeException("" + c);
		};
	}

	public static int sigi(int x) {
		return sign(x, 0);
	}

	public static int sign(int x, int n) {
		if ( x < -n )
			return -1;
		if ( x > n )
			return 1;
		return 0;
	}

	public static long sigl(long x) {
		if ( x < 0 )
			return -1;
		if ( x > 0 )
			return 1;
		return 0;
	}

	public static boolean isInt(char c) {
		return c >= '0' && c <= '9';
	}

	public static int getInt(String s, String rep) {
		String p = s.replaceAll(rep, "").trim();
		return Integer.parseInt(p);
	}

	/*
	switch (s) {
		case "R" -> x++;
		case "L" -> x--;
		case "U" -> y--;
		case "D" -> y++;
		default -> throw new AssertionError();
	}

	switch (c) {
		case '>' -> x++;
		case '<' -> x--;
		case 'v' -> y--;
		case '^' -> y++;
	}

	return switch (c) {
		case '>' -> 0;
		case 'v' -> 1;
		case '<' -> 2;
		case '^' -> 3;
		default -> throw new RuntimeException("wrong dir " + c);
	};
	 */
}
