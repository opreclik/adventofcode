package utils;

public class ParseNum {

	public int val;

	public int read(String a, int i) {
		val = a.charAt(i++) - '0';
		while (i < a.length() && Utils.isInt(a.charAt(i))) {
			val *= 10;
			val += a.charAt(i++) - '0';
		}
		return i;
	}
}
