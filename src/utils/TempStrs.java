package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class TempStrs {

	public static void main(String[] args) throws IOException {
		int ex = 1;
		String in = ex > 0 ? "ex.txt" : "xxx";

		long sum1 = 0;
		long sum2 = 0;
		String line = Files.readString(Paths.get(in));
		int[] v = Files.readAllLines(Paths.get(in)).stream().mapToInt(Integer::parseInt).toArray();
		for (String a : Files.readAllLines(Paths.get(in))) {
			Scanner sc = new Scanner(a.replaceAll("[,-]", " "));
			int j = sc.nextInt();
			int k = sc.nextInt();
			int l = sc.nextInt();
			int m = sc.nextInt();
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
