package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day05SeedRanges {

	static record Range(long a, long len) {}

	static record Mapping(long d, long s, long r) {

		long get(long v) {
			if ( v < s || v >= s + r )
				return -1;
			return v + d - s;
		}

		void map(Range ran, List<Range> dump, List<Range> newr) {
			long a = ran.a;
			long len = ran.len;
			if ( a + len <= s || a >= s + r ) {
				dump.add(ran);
				return;
			}
			if ( a < s ) {
				dump.add(new Range(a, s - a));
				a = s;
				len -= s - a;
			}
			long l = Math.min(a + len, s + r) - a;
			newr.add(new Range(a + d - s, l));
			if ( len > l )
				dump.add(new Range(a + l, len - l));
		}
	}

	public static void main(String[] args) throws IOException {
		List<String> strs = Files.readAllLines(Paths.get("05.txt"));
		Scanner sc = new Scanner(strs.getFirst());
		sc.next();
		long[] v = sc.tokens().mapToLong(Long::parseLong).toArray();

		List<Range> ran = new ArrayList<>();
		for (int i = 0; i < v.length; i += 2)
			ran.add(new Range(v[i], v[i + 1]));

		List<Mapping> maps = new ArrayList<>();
		for (String s : strs) {
			if ( s.isEmpty() ) {
				for (int i = 0; i < v.length; i++) {
					for (Mapping m : maps) {
						long k = m.get(v[i]);
						if ( k >= 0 ) {
							v[i] = k;
							break;
						}
					}
				}
				List<Range> newr = new ArrayList<>();
				for (Mapping m : maps) {
					List<Range> dump = new ArrayList<>();
					for (Range r : ran)
						m.map(r, dump, newr);
					ran = dump;
				}
				ran.addAll(newr);
				maps.clear();
				continue;
			}
			if ( s.charAt(0) <= '9' ) {
				sc = new Scanner(s);
				maps.add(new Mapping(sc.nextLong(), sc.nextLong(), sc.nextLong()));
			}
		}
		System.out.println(Arrays.stream(v).min());
		System.out.println(ran.stream().mapToLong(r -> r.a).min());
	}
}
