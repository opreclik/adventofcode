package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day15HashBoxes {

	public static void main(String[] args) throws IOException {
		List<Map<String, Integer>> m = new ArrayList<>(256);
		for (int i = 0; i < 256; i++)
			m.add(new LinkedHashMap<>());

		long sum1 = 0;
		for (String s : Files.readString(Paths.get("15.txt")).split(",")) {
			String k = "";
			int v = 0;
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				switch (c) {
					case '-' -> m.get(v).remove(k);
					case '=' -> m.get(v).put(k, s.charAt(i + 1) - '0');
				}
				k += c;
				v = (v + c) * 17 & 255;
			}
			sum1 += v;
		}
		System.out.println(sum1);

		long sum2 = 0;
		for (int i = 0; i < m.size(); i++) {
			int c = 1;
			for (var v : m.get(i).values())
				sum2 += (i + 1) * c++ * v;
		}
		System.out.println(sum2);
	}
}
