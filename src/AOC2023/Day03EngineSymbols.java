package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import utils.Utils;

public class Day03EngineSymbols {

	static int key(int x, int y) {
		return 1001 * x + y;
	}

	static class Gear {

		int x;
		int y;
		long r = 1;
		int c;

		Gear(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int hashCode() {
			return key(x, y);
		}

		public boolean equals(Object obj) {
			return obj instanceof Gear g && x == g.x && y == g.y;
		}
	}

	static HashMap<Integer, Gear> gears = new HashMap<>();

	static boolean sym(char c, int i, int r, List<Gear> g) {
		if ( c == '*' )
			g.add(gears.computeIfAbsent(key(i, r), (v) -> new Gear(i, r)));
		return !Utils.isInt(c) && c != '.';
	}

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		List<String> strs = new ArrayList<>();
		strs.add(new StringBuilder().repeat('.', 200).toString());
		for (String a : Files.readAllLines(Paths.get("03.txt")))
			strs.add('.' + a + '.');
		strs.add(strs.getFirst());

		List<Gear> gr = new ArrayList<>();
		for (int r = 1; r < strs.size() - 1; r++) {
			String s = strs.get(r);
			long val = 0;
			Set<Gear> act = new HashSet<>();
			boolean sym = false;
			for (int i = 1; i < s.length(); i++) {
				char c = s.charAt(i);
				gr.clear();
				boolean n = sym(strs.get(r - 1).charAt(i), i, r - 1, gr)
					|| sym(c, i, r, gr)
					|| sym(strs.get(r + 1).charAt(i), i, r + 1, gr);

				act.addAll(gr);
				sym = sym || n;
				if ( Utils.isInt(c) ) {
					val = val * 10 + (c - '0');
					continue;
				}
				if ( sym && val > 0 ) {
					sum1 += val;
					for (Gear g : act) {
						g.c++;
						g.r *= val;
					}
				}
				val = 0;
				sym = n;
				act.clear();
				act.addAll(gr);
			}
		}
		System.out.println(sum1);
		System.out.println(gears.values().stream().filter(g -> g.c == 2).mapToLong(g -> g.r).sum());
	}
}
