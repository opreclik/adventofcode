package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day08GraphSteps {

	static record Node(String n, String l, String r) {}

	static int go(String cmd, Map<String, Node> m, String a) {
		int ret = 0;
		while (true) {
			for (char c : cmd.toCharArray()) {
				Node n = m.get(a);
				a = c == 'L' ? n.l : n.r;
				ret++;
				if ( a.endsWith("Z") )
					return ret;
			}
		}
	}

	public static void main(String[] args) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get("08.txt"));
		String cmd = lines.getFirst();
		Map<String, Node> m = new HashMap<>();
		List<String> st = new ArrayList<>();
		for (int i = 2; i < lines.size(); i++) {
			String[] s = lines.get(i).replaceAll("[=\\(),]", "").split(" +");
			Node n = new Node(s[0], s[1], s[2]);
			m.put(s[0], n);
			if ( s[0].endsWith("A") )
				st.add(s[0]);
		}

		System.out.println(go(cmd, m, "AAA"));

		long r = cmd.length();
		for (String s : st)
			r *= go(cmd, m, s) / cmd.length();
		System.out.println(r);
	}
}
