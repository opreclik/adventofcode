package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day13Mirrors {

	static String[] trans(String[] x, int cnt, String[] y) {
		for (int i = 0; i < x[0].length(); i++) {
			StringBuilder sb = new StringBuilder(cnt);
			for (int j = 0; j < cnt; j++)
				sb.append(x[j].charAt(i));
			y[i] = sb.toString();
		}
		return y;
	}

	static int count(String[] x, int cnt, int m) {
		for (int i = 0; i < cnt - 1; i++) {
			int d = 0;
			for (int j = i, k = i + 1; j >= 0 && k < cnt && d <= m; j--, k++) {
				for (int c = 0; c < x[0].length() && d <= m; c++) {
					if ( x[k].charAt(c) != x[j].charAt(c) )
						d++;
				}
			}
			if ( d == m )
				return i + 1;
		}
		return 0;
	}

	static int mir(String[] x, int cnt, String[] y, int m) {
		return 100 * count(x, cnt, m) + count(y, x[0].length(), m);
	}

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		String[] x = new String[50];
		String[] y = new String[50];
		int cnt = 0;
		for (String a : Files.readAllLines(Paths.get("13.txt"))) {
			if ( !a.isBlank() ) {
				x[cnt++] = a;
				continue;
			}
			sum1 += mir(x, cnt, trans(x, cnt, y), 0);
			sum2 += mir(x, cnt, y, 1);
			cnt = 0;
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
