package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import utils.Utils;

public class Day14RocksOnPlatform {

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		int cnt = 0;
		int line = 0;
		int[] s = new int[100];
		int[][] m = new int[s.length][s.length];
		for (String a : Files.readAllLines(Paths.get("14.txt"))) {
			for (int i = 0; i < s.length; i++) {
				char c = a.charAt(i);
				m[line][i] = switch (c) {
					case '.' -> 0;
					case '#' -> 1;
					case 'O' -> 2;
					default -> 0;
				};
				switch (c) {
					case '.':
						s[i]++;
						break;
					case '#':
						s[i] = 0;
						break;
					case 'O':
						sum1 += s[i];
						cnt++;
						break;
				}
			}
			sum1 += cnt;
			line++;
		}
		System.out.println(sum1);

		for (int i = 1; i < 200; i++) {
			for (int d = 3; d >= 0; d--)
				while (move(m, Utils.dx[d], Utils.dy[d]));

			if ( (1_000_000_000 - i) % 14 == 0 ) {
				int load = 0;
				for (int j = 0, c = 0; j < m.length; j++, load += c)
					c += (int)Arrays.stream(m[j]).filter(v -> v == 2).count();
				System.out.println(i + " " + load);
			}
		}
	}

	static boolean move(int[][] m, int dx, int dy) {
		boolean mv = false;
		for (int r = 0; r < m.length; r++) {
			for (int c = 0; c < m.length; c++) {
				if ( m[r][c] != 2 )
					continue;
				int x = c + dx;
				if ( x < 0 || x >= m.length )
					continue;
				int y = r + dy;
				if ( y < 0 || y >= m.length )
					continue;
				if ( m[y][x] == 0 ) {
					m[y][x] = 2;
					m[r][c] = 0;
					mv = true;
				}
			}
		}
		return mv;
	}
}
