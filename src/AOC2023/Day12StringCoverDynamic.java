package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Day12StringCoverDynamic {

	static long sol(String s, int[] v) {
		s += '.';
		long[] m = new long[s.length() + 1];
		Arrays.fill(m, s.lastIndexOf('#') + 1, m.length, 1);
		for (int a = v.length - 1, e = s.length(); a >= 0; a--) {
			long[] n = new long[m.length];
			e -= v[a] + 1;
			for (int i = e, f = e + v[a]; i >= 0; i--, f--) {
				if ( s.charAt(i) != '#' )
					n[i] = n[i + 1];
				if ( s.charAt(f) != '#' && s.indexOf('.', i, f) == -1 )
					n[i] += m[f + 1];
			}
			m = n;
		}
		return m[0];
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		long sum1 = 0;
		long sum2 = 0;
		for (String a : Files.readAllLines(Paths.get("12.txt"))) {
			Scanner sc = new Scanner(a.replace(',', ' '));
			String s = sc.next();
			int[] v = sc.tokens().mapToInt(Integer::parseInt).toArray();
			sum1 += sol(s, v);
			int[] v5 = new int[v.length * 5];
			for (int i = 0, d = 0; i < 5; i++, d += v.length)
				System.arraycopy(v, 0, v5, d, v.length);
			sum2 += sol((s + '?').repeat(4) + s, v5);
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
