package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Day11Galaxies {

	static int[] rows;
	static int[] cols;

	static record Galaxy(int x, int y) {

		long rx(long m) {
			return x + cols[x] * m;
		}

		long ry(long m) {
			return y + rows[y] * m;
		}
	}

	static int emptyCol(int x, List<String> strs) {
		for (int i = 0; i < rows.length; i++) {
			if ( strs.get(i).charAt(x) == '#' )
				return 0;
		}
		return 1;
	}

	static long dist(Galaxy i, Galaxy j, long m) {
		return Math.abs(i.rx(m) - j.rx(m)) + Math.abs(i.ry(m) - j.ry(m));
	}

	public static void main(String[] args) throws IOException {
		List<String> strs = Files.readAllLines(Paths.get("11.txt"));
		rows = new int[strs.size()];
		cols = new int[strs.getFirst().length()];

		List<Galaxy> gals = new ArrayList<>();
		for (int y = 0; y < strs.size(); y++) {
			String s = strs.get(y);
			int g = gals.size();
			for (int p = 0; true; p++) {
				p = s.indexOf('#', p);
				if ( p == -1 )
					break;
				gals.add(new Galaxy(p, y));
			}
			if ( y > 0 )
				rows[y] = rows[y - 1];
			if ( g == gals.size() )
				rows[y]++;
		}
		cols[0] = emptyCol(0, strs);
		for (int i = 1; i < cols.length; i++)
			cols[i] = cols[i - 1] + emptyCol(i, strs);

		long sum1 = 0;
		long sum2 = 0;
		for (int i = 0; i < gals.size(); i++) {
			Galaxy gi = gals.get(i);
			for (int j = i + 1; j < gals.size(); j++) {
				Galaxy gj = gals.get(j);
				sum1 += dist(gi, gj, 1);
				sum2 += dist(gi, gj, 999_999);
			}
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
