package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import utils.Utils;

public class Day10PipeMaze {

	public static void main(String[] args) throws IOException {
		List<String> strs = Files.readAllLines(Paths.get("10.txt")).stream().map(s -> '.' + s + '.').toList();
		int sx = 0;
		int sy = 0;
		for (String s : strs) {
			int p = s.indexOf('S');
			if ( p != -1 ) {
				sx = p;
				break;
			}
			sy++;
		}

		int[][] m = new int[150][];
		m['-'] = new int[]{-1, 0, 1, 0};
		m['|'] = new int[]{0, 1, 0, -1};
		m['F'] = new int[]{1, 0, 0, 1};
		m['L'] = new int[]{1, 0, 0, -1};
		m['7'] = new int[]{-1, 0, 0, 1};
		m['J'] = new int[]{-1, 0, 0, -1};

		int x = 0;
		int y = 0;
		for (int i = 0; i < 4; i++) {
			x = sx + Utils.dx[i];
			y = sy + Utils.dy[i];
			int[] d = m[strs.get(y).charAt(x)];
			if ( d == null )
				continue;
			if ( sx == x + d[0] && sy == y + d[1] )
				break;
			if ( sx == x + d[2] && sy == y + d[3] )
				break;
		}

		int px = sx;
		int py = sy;
		int cnt = 1;
		double area = x * py - y * px;
		while (x != sx || y != sy) {
			int[] d = m[strs.get(y).charAt(x)];
			int i = (x + d[0] == px && y + d[1] == py) ? 2 : 0;
			px = x;
			py = y;
			x += d[i];
			y += d[i + 1];
			area += x * py - y * px;
			cnt++;
		}
		System.out.println(cnt / 2);
		System.out.println((Math.abs(area) - cnt) / 2 + 1);
	}
}
