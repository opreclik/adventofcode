package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Day02ColorCubes {

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		for (String a : Files.readAllLines(Paths.get("02.txt"))) {
			Scanner sc = new Scanner(a.replaceAll("[,:;]", " "));
			sc.next();

			int i = sc.nextInt();
			int r = 0;
			int g = 0;
			int b = 0;

			while (sc.hasNext()) {
				int v = sc.nextInt();
				String c = sc.next();
				boolean ok = switch (c) {
					case "red" -> v <= 12;
					case "green" -> v <= 13;
					case "blue" -> v <= 14;
					default -> throw new RuntimeException();
				};
				if ( !ok )
					i = 0;

				switch (c) {
					case "red" -> r = Math.max(r, v);
					case "green" -> g = Math.max(g, v);
					case "blue" -> b = Math.max(b, v);
				}
			}
			sum1 += i;
			sum2 += r * b * g;
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
