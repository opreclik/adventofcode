package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import utils.Utils;

public class Day01NumParsing {

	static String[] n = new String[]{"0", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

	static int get(String s, int p, boolean txt) {
		if ( Utils.isInt(s.charAt(p)) )
			return s.charAt(p) - '0';
		if ( txt ) {
			for (int i = 0; i < n.length; i++) {
				if ( s.startsWith(n[i], p) )
					return i;
			}
		}
		return -1;
	}

	static public int read(String s, boolean txt) {
		int a = -1;
		int b = -1;
		for (int i = 0; i < s.length(); i++) {
			int v = get(s, i, txt);
			if ( v == -1 )
				continue;
			if ( a == -1 )
				a = v;
			b = v;
		}
		return a * 10 + b;
	}

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		for (String a : Files.readAllLines(Paths.get("01.txt"))) {
			sum1 += read(a, false);
			sum2 += read(a, true);
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
