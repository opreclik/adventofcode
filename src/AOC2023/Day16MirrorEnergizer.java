package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import utils.Utils;

public class Day16MirrorEnergizer {

	static record Dir(int x, int y, int d) {}

	static Set<Integer> dirs = new HashSet<>();

	static void add(int x, int y, int d, Map<Integer, Dir> nw) {
		d %= 4;
		int k = (x + 137 * y) * 7 + d;
		if ( !dirs.contains(k) ) {
			nw.put(k, new Dir(x, y, d));
			dirs.add(k);
		}
	}

	static void split(int x, int y, int d, int t, Map<Integer, Dir> nw) {
		if ( (d & 1) == t ) {
			add(x, y, d + 1, nw);
			add(x, y, d + 3, nw);
		}
		else
			add(x, y, d, nw);
	}

	static long go(List<String> s, Dir init) throws RuntimeException {
		Map<Integer, Dir> w = new HashMap<>();
		dirs.clear();
		w.put(0, init);
		while (!w.isEmpty()) {
			Map<Integer, Dir> nw = new HashMap<>();
			for (var p : w.values()) {
				int x = p.x + Utils.dx[p.d];
				int y = p.y + Utils.dy[p.d];
				if ( x < 0 || x >= s.size() || y < 0 || y >= s.size() )
					continue;
				switch (s.get(y).charAt(x)) {
					case '.' -> add(x, y, p.d, nw);
					case '/' -> add(x, y, p.d + ((p.d & 1) == 0 ? 3 : 1), nw);
					case '\\' -> add(x, y, p.d + ((p.d & 1) == 0 ? 1 : 3), nw);
					case '|' -> split(x, y, p.d, 0, nw);
					case '-' -> split(x, y, p.d, 1, nw);
				}
			}
			w = nw;
		}
		return dirs.stream().mapToInt(i -> i / 7).distinct().count();
	}

	public static void main(String[] args) throws IOException {
		List<String> s = Files.readAllLines(Paths.get("16.txt"));

		System.out.println(go(s, new Dir(-1, 0, 0)));

		long m = 0;
		for (int i = 0; i < s.size(); i++) {
			m = Math.max(go(s, new Dir(-1, i, 0)), m);
			m = Math.max(go(s, new Dir(i, -1, 1)), m);
			m = Math.max(go(s, new Dir(s.size(), i, 2)), m);
			m = Math.max(go(s, new Dir(i, s.size(), 3)), m);
		}
		System.out.println(m);
	}
}
