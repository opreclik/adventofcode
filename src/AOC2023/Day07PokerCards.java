package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day07PokerCards {

	static int[] h = new int[150];

	static long type2(String s) {
		Arrays.fill(h, 0);
		int j = 0;
		int m = 20;
		for (char c : s.toCharArray()) {
			if ( c == 'J' ) {
				j++;
				continue;
			}
			if ( ++h[c] > h[m] )
				m = c;
		}
		h[m] += j;
		for (int i : h)
			h[i]++;
		int r = 0;
		for (int i = 5; i >= 1; i--)
			r = r * 10 + h[i];
		return r;
	}

	static long value(String s) {
		long r = 0;
		for (char c : s.toCharArray()) {
			long v = switch (c) {
				case 'T' -> 10;
				case 'J' -> 1;
				case 'Q' -> 12;
				case 'K' -> 13;
				case 'A' -> 14;
				default -> c - '0';
			};
			r = r * 100 + v;
		}
		return r;
	}

	static record Card(String c, long bid, long t, long v) {}

	public static void main(String[] args) throws IOException {
		List<Card> cards = new ArrayList<>();
		for (String a : Files.readAllLines(Paths.get("07.txt"))) {
			Scanner sc = new Scanner(a);
			String s = sc.next();
			cards.add(new Card(s, sc.nextLong(), type2(s), value(s)));
		}
		cards.sort(Comparator.comparingLong((Card c) -> c.t).thenComparingLong(c -> c.v));
		int i = 1;
		long sum = 0;
		for (Card c : cards)
			sum += c.bid * i++;
		System.out.println(sum);
	}
}
