package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import utils.Utils;

public class Day17GridHeat {

	static List<String> s;

	static record Pos(int x, int y, int d, int c, int v) {}

	static void gen(Pos p, int d, int c, Map<Integer, Pos> b, int min) {
		int x = p.x + Utils.dx[d];
		int y = p.y + Utils.dy[d];
		if ( x < 0 || x >= s.get(0).length() || y < 0 || y >= s.size() )
			return;
		int v = p.v + s.get(y).charAt(x) - '0';
		if ( v >= min )
			return;
		int k = c + 11 * (d + 5 * (x + 149 * y));
		Pos e = b.get(k);
		if ( e == null || e.v > v )
			b.put(k, new Pos(x, y, d, c, v));
	}

	static int bfs(Collection<Pos> w, int t1, int t2) {
		int min = s.get(0).length() * 10 * 2;
		while (!w.isEmpty()) {
			Map<Integer, Pos> b = new HashMap<>();
			for (Pos p : w) {
				if ( p.c < t2 )
					gen(p, p.d, p.c + 1, b, min);
				if ( p.c >= t1 ) {
					if ( p.y == s.size() - 1 && p.x == s.get(0).length() - 1 )
						min = Math.min(min, p.v);
					gen(p, (p.d + 1) % 4, 0, b, min);
					gen(p, (p.d + 3) % 4, 0, b, min);
				}
			}
			w = b.values();
		}
		return min;
	}

	public static void main(String[] args) throws IOException {
		s = Files.readAllLines(Paths.get("17.txt"));
		List<Pos> w = new ArrayList<>();
		w.add(new Pos(0, 0, 0, -1, 0));
		w.add(new Pos(0, 0, 1, -1, 0));
		System.out.println(bfs(w, 0, 2));
		System.out.println(bfs(w, 3, 9));
	}
}
