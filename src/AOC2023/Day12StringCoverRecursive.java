package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Day12StringCoverRecursive {

	static long[] m = new long[5000];

	static long sol(String s, int[] v, int[] e, int p, int a) {
		if ( a == v.length )
			return s.indexOf('#', p) == -1 ? 1 : 0;

		int k = 37 * p + a;
		if ( m[k] < 0 ) {
			m[k] = 0;
			for (int i = p, f = p + v[a]; i <= e[a]; i++, f++) {
				if ( s.charAt(f) != '#' && s.indexOf('.', i, f) == -1 )
					m[k] += sol(s, v, e, f + 1, a + 1);
				if ( s.charAt(i) == '#' )
					break;
			}
		}
		return m[k];
	}

	static long sol(String s, int[] v) {
		Arrays.fill(m, -1);
		int[] e = new int[v.length + 1];
		e[v.length] = s.length() + 1;
		for (int i = v.length - 1; i >= 0; i--)
			e[i] = e[i + 1] - (v[i] + 1);
		return sol(s + '.', v, e, 0, 0);
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		long sum1 = 0;
		long sum2 = 0;
		for (String a : Files.readAllLines(Paths.get("12.txt"))) {
			Scanner sc = new Scanner(a.replace(',', ' '));
			String s = sc.next();
			int[] v = sc.tokens().mapToInt(Integer::parseInt).toArray();
			sum1 += sol(s, v);
			int[] v5 = new int[v.length * 5];
			for (int i = 0, d = 0; i < 5; i++, d += v.length)
				System.arraycopy(v, 0, v5, d, v.length);
			sum2 += sol((s + '?').repeat(4) + s, v5);
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
