package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Day09SequenceExtrapolation {

	static long v;

	static long m(long i) {
		long r = i - v;
		v = i;
		return r;
	}

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		for (String a : Files.readAllLines(Paths.get("09.txt"))) {
			List<Long> vs = new Scanner(a).tokens().mapToLong(Long::parseLong).boxed().toList();
			long f = 1;
			while (vs.stream().anyMatch(i -> i != 0)) {
				sum1 += vs.getLast();
				sum2 += f * vs.getFirst();
				v = vs.getFirst();
				vs = vs.stream().skip(1).mapToLong(Day09SequenceExtrapolation::m).boxed().toList();
				f = -f;
			}
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
