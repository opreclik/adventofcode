package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.IntFunction;
import java.util.stream.Stream;

public class Day19RulesAndParts {

	static record Rule(int i, IntFunction<Boolean> f, int v, String t) {}

	static record Int(int a, int b) {}

	static Map<String, List<Rule>> rs = new HashMap<>();

	static long sum2(Int[] c, String n) {
		if ( n.equals("R") )
			return 0;
		if ( n.equals("A") )
			return Arrays.stream(c).mapToLong(r -> r.b - r.a + 1).reduce(1, (a, b) -> a * b);
		long v = 0;
		for (Rule r : rs.get(n)) {
			Int i = c[r.i];
			boolean a = r.f.apply(i.a);
			boolean b = r.f.apply(i.b);
			if ( a && b )
				return v + sum2(c, r.t);
			if ( a != b ) {
				Int[] d = c.clone();
				d[r.i] = new Int(a ? i.a : r.v + 1, b ? i.b : r.v - 1);
				v += sum2(d, r.t);
				c[r.i] = new Int(a ? r.v : i.a, b ? r.v : i.b);
			}
		}
		return v;
	}

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		for (String s : Files.readAllLines(Paths.get("19.txt"))) {
			if ( s.isBlank() )
				continue;
			int b = s.indexOf('{');
			if ( b == 0 ) {
				int[] p = Stream.of(s.replaceAll("[xmas{}=]", "").split(",")).mapToInt(Integer::parseInt).toArray();
				String n = "in";
				while (n.length() > 1)
					n = rs.get(n).stream().filter(r -> r.f.apply(p[r.i])).findFirst().get().t;
				if ( n.equals("A") )
					sum1 += Arrays.stream(p).sum();
				continue;
			}
			rs.put(s.substring(0, b), Arrays.stream(s.substring(b + 1, s.length() - 1).split(",")).map(r -> {
				var p = r.split(":");
				if ( p.length == 1 )
					return new Rule(0, i -> true, 0, r);
				int i = switch (r.charAt(0)) {
					case 'x' -> 0;
					case 'm' -> 1;
					case 'a' -> 2;
					case 's' -> 3;
					default -> 0;
				};
				int v = Integer.parseInt(p[0].substring(2));
				return new Rule(i, r.charAt(1) == '>' ? x -> x > v : x -> x < v, v, p[1]);
			}).toList());
		}
		System.out.println(sum1);
		Int[] r = new Int[4];
		Arrays.fill(r, new Int(1, 4000));
		System.out.println(sum2(r, "in"));
	}
}
