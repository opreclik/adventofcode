package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Day04Counts {

	public static void main(String[] args) throws IOException {
		int[] c = new int[300];
		System.out.println(Files.readAllLines(Paths.get("04.txt")).stream().map(s -> new Scanner(s.replace(':', ' '))).mapToInt(s -> {
			s.next();
			int p = s.nextInt(), i = p;
			c[p]++;
			var n = s.tokens().takeWhile(t -> !t.equals("|")).mapToInt(Integer::parseInt).boxed().toList();
			while (s.hasNext())
				if ( n.contains(s.nextInt()) )
					c[++i] += c[p];
			return 1 << i - p >> 1;
		}).sum());
		System.out.println(Arrays.stream(c).sum());
	}
}
