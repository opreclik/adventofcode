package AOC2023;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import utils.Utils;

public class Day18DigArea {

	public static void main(String[] args) throws IOException {
		long p = 2, x = 0, y = 0, a = 0;
		for (String s : Files.readAllLines(Paths.get("18.txt"))) {
			String[] r = s.split(" ");
			int d = Utils.a2d(r[0].charAt(0));
			int m = Integer.parseInt(r[1]);
			d = r[2].charAt(7) - '0';
			m = Integer.parseInt(r[2].substring(2, 7), 16);

			long px = x;
			long py = y;
			x += Utils.dx[d] * m;
			y += Utils.dy[d] * m;
			a += x * py - y * px;
			p += m;
		}
		System.out.println((Math.abs(a) + p) / 2);
	}
}
