package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day03 {

	static int prio(char a) {
		if ( a >= 'a' ) {
			return a - 'a' + 1;
		}
		return a - 'A' + 27;
	}

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		int d = 0;
		boolean ar[][] = new boolean[3][200];
		for (String a : Files.readAllLines(Paths.get("03.txt"))) {
			boolean b[] = new boolean[300];
			int len = a.length();
			int half = len / 2;
			for (int i = 0; i < half; i++) {
				b[a.charAt(i)] = true;
			}

			for (int i = half; i < len; i++) {
				if ( b[a.charAt(i)] ) {
					sum1 += prio(a.charAt(i));
					break;
				}
			}
			for (int i = 0; i < len; i++) {
				ar[d][a.charAt(i)] = true;
			}
			d++;
			if ( d == 3 ) {
				for (char i = 30; i < 150; i++) {
					if ( ar[0][i] && ar[1][i] && ar[2][i] ) {
						sum2 += prio(i);
					}
				}
				ar = new boolean[3][200];
				d = 0;
			}
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
