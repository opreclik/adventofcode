package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import utils.Utils;

public class Day09Rope {

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "09.txt";

		Set<Long> visited = new HashSet<>();
		long x[] = new long[10];
		long y[] = new long[10];
		for (String a : Files.readAllLines(Paths.get(in))) {
			Scanner sc = new Scanner(a);
			String s = sc.next();
			int m = sc.nextInt();
			for (int j = 0; j < m; j++) {
				switch (s) {
					case "R" -> x[0]++;
					case "L" -> x[0]--;
					case "U" -> y[0]--;
					case "D" -> y[0]++;
					default -> throw new AssertionError();
				}
				for (int i = 1; i < 10; i++) {
					long dx = x[i - 1] - x[i];
					long dy = y[i - 1] - y[i];
					long ax = Math.abs(dx);
					long ay = Math.abs(dy);
					boolean diag = (ax + ay) > 2;
					long mx = diag || ax > 1 ? Utils.sigl(dx) : 0;
					long my = diag || ay > 1 ? Utils.sigl(dy) : 0;
					x[i] += mx;
					y[i] += my;
				}
				visited.add(x[9] + 100000 * y[9]);
			}
		}
		System.out.println(visited.size());
	}
}
