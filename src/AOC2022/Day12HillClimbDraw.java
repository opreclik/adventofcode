package AOC2022;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JFrame;
import javax.swing.JPanel;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class Day12HillClimbDraw {

	static class Coord {

		int x;
		int y;

		Coord(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int hashCode() {
			return x + 1001 * y;
		}

		public boolean equals(Object obj) {
			if ( this == obj )
				return true;
			if ( obj == null )
				return false;
			if ( getClass() != obj.getClass() )
				return false;
			final Coord other = (Coord)obj;
			if ( this.x != other.x )
				return false;
			return this.y == other.y;
		}

	}

	static class Draw extends JPanel {

		int row;
		int col;
		int[][] p;
		int max;

		public Draw(int row, int col, int[][] p, int m) {
			this.row = row;
			this.col = col;
			this.p = p;
			this.max = m;
		}

		protected void paintComponent(Graphics g) {
			int f = 4;
			for (int j = 0; j < row; j++) {
				for (int i = 0; i < col; i++) {
					float v = (float)(p[j][i] / (float)max * 0.9 + 0.1);
					if ( v > 1 )
						v = 1;
					Color c = new Color(v, v, v, 1);
					g.setColor(c);
					g.fillRect(i * f, j * f, f, f);
				}
			}
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "12.txt";

		int[][] m = new int[50][170];
		int[][] p = new int[50][170];
		int[] dx = {0, 1, 0, -1};
		int[] dy = {-1, 0, 1, 0};

		int row = 1;
		int col = 1;
		int mx = 0;
		Coord end = null;
		Set<Coord> w = new HashSet<>();

		for (String a : Files.readAllLines(Paths.get(in))) {
			for (byte b : a.getBytes()) {
				p[row][col] = -1;
				if ( b == 'S' || b == 'a' ) {
					w.add(new Coord(row, col));
					b = 'a';
					p[row][col] = 0;
				}
				if ( b == 'E' ) {
					end = new Coord(row, col);
					b = 'z';
				}
				m[row][col++] = b;
			}
			row++;
			mx = Math.max(col, mx);
			col = 1;
		}

		int max = 0;
		while (!w.isEmpty()) {
			Set<Coord> n = new HashSet<>();
			for (Coord c : w) {
				int a = p[c.x][c.y];
				for (int i = 0; i < 4; i++) {
					int x = c.x + dx[i];
					int y = c.y + dy[i];
					if ( p[x][y] >= 0 )
						continue;
					int diff = m[x][y] - m[c.x][c.y];
					if ( diff > 1 )
						continue;
					p[x][y] = a + 1;
					max = Math.max(max, a + 1);
					n.add(new Coord(x, y));
				}
			}
			w = n;
		}

		System.out.println(p[end.x][end.y]);

		JFrame main = new JFrame();
		main.setMinimumSize(new Dimension(1000, 300));
		main.add(new Draw(row, mx, p, max));
		main.setVisible(true);
		main.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
}
