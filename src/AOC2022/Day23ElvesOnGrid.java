package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Day23ElvesOnGrid {

	static int[][] dirs = {
		{-1, -1, 1, -1, 0, -1},
		{-1, 1, 1, 1, 0, 1},
		{-1, -1, -1, 1, -1, 0},
		{1, -1, 1, 1, 1, 0}
	};

	static int[] all = {-1, -1, 0, -1, 1, -1, -1, 0, 1, 0, -1, 1, 0, 1, 1, 1};

	static class Elf {

		int x;
		int y;
		int nx;
		int ny;

		Elf(int x, int y) {
			this.x = x;
			this.y = y;
		}

		int keyn() {
			return key(nx, ny);
		}

		static int key(int a, int b) {
			return a * 10000 + b;
		}

		void go() {
			x = nx;
			y = ny;
		}

		boolean checkDirs(int[] ar, Set<Integer> p) {
			for (int i = 0; i < ar.length; i += 2) {
				nx = x + ar[i];
				ny = y + ar[i + 1];
				if ( p.contains(keyn()) )
					return false;
			}
			return true;
		}

		boolean move(int d, Set<Integer> pos) {
			if ( checkDirs(all, pos) )
				return false;
			for (int j = 0; j < 4; j++) {
				int dir = (j + d) & 3;
				if ( checkDirs(dirs[dir], pos) )
					return true;
			}
			return false;
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "23.txt";

		List<String> lines = Files.readAllLines(Paths.get(in));
		List<Elf> elves = new ArrayList<>();
		for (int row = 0; row < lines.size(); row++) {
			String a = lines.get(row);
			for (int i = 0; i < a.length(); i++) {
				if ( a.charAt(i) == '#' )
					elves.add(new Elf(i, row));
			}
		}

		for (int i = 1; true; i++) {
			Set<Integer> pos = elves.stream().map(e -> Elf.key(e.x, e.y)).collect(Collectors.toSet());
			Map<Integer, Integer> p = new HashMap<>();
			List<Elf> moving = new ArrayList<>();
			for (Elf e : elves) {
				if ( e.move(i - 1, pos) ) {
					moving.add(e);
					p.compute(e.keyn(), (k, v) -> v == null ? 1 : v + 1);
				}
			}
			moving.stream().filter(e -> p.get(e.keyn()) == 1).forEach(Elf::go);

			if ( moving.isEmpty() ) {
				System.out.println(i);
				break;
			}

			if ( i == 10 ) {
				Elf f = elves.get(0);
				int ax = f.x, bx = f.x, ay = f.y, by = f.y;
				for (Elf e : elves) {
					ax = Math.min(ax, e.x);
					bx = Math.max(bx, e.x);
					ay = Math.min(ay, e.y);
					by = Math.max(by, e.y);
				}
				System.out.println((bx - ax + 1) * (by - ay + 1) - elves.size());
			}
		}
	}
}
