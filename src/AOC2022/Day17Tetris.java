package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day17Tetris {

	static int[][] shapes = new int[][]{
		{0, 0, 1, 0, 2, 0, 3, 0},
		{1, 0, 0, 1, 1, 1, 2, 1, 1, 2},
		{0, 0, 1, 0, 2, 0, 2, 1, 2, 2},
		{0, 0, 0, 1, 0, 2, 0, 3},
		{0, 0, 1, 0, 0, 1, 1, 1}
	};

	static List<int[]> cave = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		int ex = 1;
		String in = ex > 0 ? "ex.txt" : "17.txt";

		int h = 0;
		int w = 0;
		String a = Files.readAllLines(Paths.get(in)).get(0);
		for (int i = 0; i < 10000; i++) {
			cave.add(new int[7]);
		}
		long all = 1000000000000L;
		Map<Integer, Integer> hei = new HashMap<>();
		Map<Integer, Integer> it = new HashMap<>();
		int end = 2022;
		long inch = 0;
		for (int i = 0; i < end; i++) {
			int r = i % 5;
			int key = w << 3 | r;
			if ( inch == 0 && hei.containsKey(key) ) {
				long diffh = h - hei.get(key);
				long diffi = i - it.get(key);
				long rem = all - i;
				long iter = rem / diffi;
				inch = iter * diffh;
				i = r - 1;
				end = (int)(rem - iter * diffi) + r;
				continue;
			}
			else if ( i > 224 ) {
				hei.put(key, h);
				it.put(key, i);
			}
			int[] rock = shapes[r];
			int x = 2;
			int y = h + 4;
			while (true) {
				char c = a.charAt(w++);
				if ( w == a.length() )
					w = 0;
				int dx = c == '>' ? 1 : -1;
				if ( test(rock, x + dx, y) )
					x += dx;
				if ( test(rock, x, y - 1) ) {
					y--;
					continue;
				}
				for (int j = 0; j < rock.length; j += 2) {
					int rx = rock[j] + x;
					int ry = rock[j + 1] + y;
					cave.get(ry)[rx] = 1;
					h = Math.max(ry, h);
				}
				break;
			}
		}
		System.out.println(h);
		System.out.println(h + inch);
		/*
		for (int i = h; i >= 0; i--) {
			int[] row = cave.get(i);
			System.out.print("|");
			for (int j : row) {
				System.out.print(j != 0 ? "#" : " ");
			}
			System.out.println("|");
		}
		 */
	}

	private static boolean test(int[] rock, int x, int y) {
		if ( x < 0 )
			return false;
		if ( y < 1 )
			return false;
		for (int i = 0; i < rock.length; i += 2) {
			int a = rock[i] + x;
			if ( a > 6 )
				return false;
			int b = rock[i + 1] + y;
			if ( cave.get(b)[a] != 0 )
				return false;
		}
		return true;
	}
}
