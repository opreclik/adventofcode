package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import utils.Utils;

public class Day25Base5 {

	static String map = "=-012";

	static final int LEN = 30;
	static final byte[] sum = new byte[LEN];
	static int m = LEN;

	static void add(String s) {
		for (int i = LEN - 1, p = s.length() - 1, c = 0; p >= 0 || c != 0; i--, p--) {
			sum[i] += c;
			if ( p >= 0 )
				sum[i] += map.indexOf(s.charAt(p)) - 2;
			c = Utils.sign(sum[i], 2);
			sum[i] -= c * 5;
			m = Math.min(i, m);
		}
	}

	public static void main(String[] args) throws IOException {
		Files.readAllLines(Paths.get("25.txt")).forEach(Day25Base5::add);
		for (int i = m; i < LEN; i++)
			System.out.print(map.charAt(sum[i] + 2));
	}
}
