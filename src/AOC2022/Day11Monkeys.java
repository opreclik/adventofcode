package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;

public class Day11Monkeys {

	static class Monkey {

		List<Long> items = new ArrayList<>();
		Function<Long, Long> func;
		long div;
		int pos;
		int neg;

		long ins;

		Monkey(Function<Long, Long> func, int div, int pos, int neg) {
			this.func = func;
			this.div = div;
			this.pos = pos;
			this.neg = neg;
		}
	}

	static Function<Long, Long> getFunc(String op) {
		char c = op.charAt(0);
		op = op.substring(1).trim();
		if ( op.equals("old") )
			return x -> x * x;

		long n = Integer.parseInt(op);
		if ( c == '*' ) {
			return x -> x * n;
		}
		return x -> x + n;
	}

	static Pattern num = Pattern.compile("\\d+");

	static int getInt(String s) {
		return Integer.parseInt(new Scanner(s).findInLine(num));
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "11.txt";
		List<String> lines = Files.readAllLines(Paths.get(in));

		List<Monkey> monkeys = new ArrayList<>();
		long fact = 1;
		for (int i = 0; i < lines.size(); i += 2) {
			String items = lines.get(++i);
			String func = lines.get(++i).replace("Operation: new = old ", "").trim();
			int div = getInt(lines.get(++i));
			int pos = getInt(lines.get(++i));
			int neg = getInt(lines.get(++i));

			Monkey m = new Monkey(getFunc(func), div, pos, neg);
			new Scanner(items)
				.findAll(num)
				.mapToLong(res -> Long.valueOf(res.group()))
				.forEachOrdered(m.items::add);
			monkeys.add(m);
			fact *= div;
		}
		System.out.println("fact: " + fact);
		for (int i = 0; i < 10000; i++) {
			for (Monkey m : monkeys) {
				for (Long it : m.items) {
					long v = m.func.apply(it) % fact;
					int idx = (v % m.div) == 0 ? m.pos : m.neg;
					monkeys.get(idx).items.add(v);
					m.ins++;
				}
				m.items.clear();
			}
		}
		long v = monkeys.stream()
			.map(m -> m.ins)
			.sorted(Comparator.reverseOrder())
			.limit(2)
			.reduce((a, b) -> a * b).get();
		System.out.println(v);
	}
}
