package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import utils.Utils;

public class Day15Beacons {

	static class Int {

		int a;
		int b;

		Int(int a, int b) {
			this.a = a;
			this.b = b;
		}
	}

	static class Beacon {

		int sx;
		int sy;
		int dist;

		Beacon(int sx, int sy, int dist) {
			this.sx = sx;
			this.sy = sy;
			this.dist = dist;
		}
	}

	static List<Int> getIntervals(List<Beacon> ar, int row) {
		List<Int> ret = new ArrayList<>();
		for (Beacon b : ar) {
			int rem = b.dist - Math.abs(row - b.sy);
			if ( rem >= 0 )
				ret.add(new Int(b.sx - rem, b.sx + rem));
		}
		return ret;
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "15.txt";

		int min = 0;
		int max = 0;
		List<Beacon> be = new ArrayList<>();
		for (String a : Files.readAllLines(Paths.get(in))) {
			String[] sp = a.split(":");
			String[] c = sp[0].split(",");
			int sx = Utils.getInt(c[0], "Sensor at x=");
			int sy = Utils.getInt(c[1], "y=");

			c = sp[1].split(",");
			int bx = Utils.getInt(c[0], "closest beacon is at x=");
			int by = Utils.getInt(c[1], "y=");

			int dist = Math.abs(bx - sx) + Math.abs(by - sy);
			be.add(new Beacon(sx, sy, dist));
			min = Math.min(min, sx - dist);
			max = Math.max(max, sx + dist);
		}

		int y = ex > 0 ? 10 : 2000000;
		int[] row = new int[max - min + 1];
		for (Int t : getIntervals(be, y)) {
			for (int i = t.a; i <= t.b; i++) {
				row[i - min] = 1;
			}
		}
		System.out.println(Arrays.stream(row).sum() - 1);

		int s = ex > 0 ? 20 : 4000000;
		for (int r = 0; r <= s; r++) {
			List<Int> ints = getIntervals(be, r);
			for (int x = 0; x <= s;) {
				int ox = x;
				for (Int i : ints) {
					if ( i.a <= x && i.b >= x ) {
						x = i.b + 1;
					}
				}
				if ( ox == x ) {
					System.out.println(x + " " + r);
					System.out.println(x * 4000000L + r);
					x++;
				}
			}
		}
	}
}
