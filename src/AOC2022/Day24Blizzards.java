package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import utils.Utils;

public class Day24Blizzards {

	static class Pos {

		int x;
		int y;

		Pos(int x, int y) {
			this.x = x;
			this.y = y;
		}

		int key() {
			return key(x, y);
		}

		static int key(int x, int y) {
			return (x << 5) + y;
		}
	}

	static int mx;
	static int my;

	static class Blizz extends Pos {

		int d;

		Blizz(int x, int y, char c) {
			super(x, y);
			d = getDir(c);
		}

		void move() {
			x += Utils.dx[d];
			y += Utils.dy[d];
			if ( x == 0 )
				x = mx - 1;
			if ( x == mx )
				x = 1;
			if ( y == 0 )
				y = my - 1;
			if ( y == my )
				y = 1;
		}

		static int getDir(char c) {
			return switch (c) {
				case '>' -> 0;
				case 'v' -> 1;
				case '<' -> 2;
				case '^' -> 3;
				default -> throw new RuntimeException("wrong dir " + c);
			};
		}
	}

	static class Wave {

		Map<Integer, Pos> m = new HashMap<>();

		Wave add(Pos p) {
			m.put(p.key(), p);
			return this;
		}

		Wave gen(Wave p, Set<Integer> b, int endx, int endy) {
			for (Pos v : p.m.values()) {
				for (int d = 0; d < 4; d++) {
					int x = v.x + Utils.dx[d];
					int y = v.y + Utils.dy[d];
					if ( x <= 0 || x >= mx || y <= 0 || y >= my )
						continue;
					int k = Pos.key(x, y);
					if ( !b.contains(k) ) {
						if ( y == endy && x == endx )
							return null;
						m.put(k, new Pos(x, y));
					}
				}
				if ( !b.contains(v.key()) )
					add(v);
			}
			return this;
		}
	}

	static List<Blizz> blizzards = new ArrayList<>();

	static int sim(Wave m, int endx, int endy) {
		for (int i = 1; true; i++) {
			blizzards.stream().forEach(Blizz::move);
			Set<Integer> blizpos = blizzards.stream().map(Blizz::key).collect(Collectors.toSet());
			m = new Wave().gen(m, blizpos, endx, endy);
			if ( m == null )
				return i;
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "24.txt";

		List<String> lines = Files.readAllLines(Paths.get(in));
		my = lines.size() - 1;
		for (int i = 1; i < my; i++) {
			String a = lines.get(i);
			mx = a.length() - 1;
			for (int j = 1; j < mx; j++) {
				char c = a.charAt(j);
				if ( c != '.' )
					blizzards.add(new Blizz(j, i, c));
			}
		}

		for (int i = 1, m = 1, x = 1, y = 1; i < 4; i++) {
			Pos p = new Pos(x, (i & 1) == 1 ? 0 : my);
			x = mx - x;
			y = my - y;
			m += sim(new Wave().add(p), x, y);
			System.out.println("step " + i + " min " + m);
		}
	}
}
