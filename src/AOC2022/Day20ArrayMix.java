package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day20ArrayMix {

	static int next(int idx, long v, int s) {
		if ( v == 0 )
			return idx;
		long ret = idx + v;
		if ( ret >= s ) {
			ret %= (s - 1);
		}
		if ( ret < 0 ) {
			long cnt = ret / (s - 1);
			ret += (1 - cnt) * (s - 1);
			ret %= (s - 1);
		}
		return (int)ret;
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "20.txt";

		long[] x = new long[5100];
		long[] y = new long[5100];
		int[] id = new int[5100];
		int[] rev = new int[5100];
		long sum1 = 0;
		int pos = 0;
		int zero = 0;
		for (String a : Files.readAllLines(Paths.get(in))) {
			long v = Integer.parseInt(a.trim());
			if ( v == 0 )
				zero = pos;
			v *= 811589153;
			x[pos] = y[pos] = v;
			rev[pos] = id[pos] = pos;
			pos++;
		}
		for (int k = 0; k < 10; k++) {
			for (int i = 0; i < pos; i++) {
				long v = x[i];
				int idx = (id[i] + pos) % pos;
				int t = next(idx, v, pos);
				if ( t > idx ) {
					for (int j = idx; j < t; j++) {
						y[j] = y[j + 1];
						rev[j] = rev[j + 1];
						id[rev[j]]--;
					}
				}
				else if ( t < idx ) {
					for (int j = idx; j > t; j--) {
						y[j] = y[j - 1];
						rev[j] = rev[j - 1];
						id[rev[j]]++;
					}
				}
				y[t] = v;
				id[i] = t;
				rev[t] = i;
			}
		}
		int idx = (id[zero] + pos) % pos;
		for (int j = 1000; j < 4000; j += 1000)
			sum1 += y[(idx + j) % pos];
		System.out.println(sum1);
	}
}
