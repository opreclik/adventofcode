package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day05Stacks {

	public static void main(String[] args) throws IOException {
		List<LinkedList<Character>> stacks = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			stacks.add(new LinkedList<>());
		}
		int line = 0;
		for (String a : Files.readAllLines(Paths.get("05.txt"))) {
			if ( line++ < 8 ) {
				for (int i = 1, st = 1; i < a.length(); i += 4, st++) {
					char c = a.charAt(i);
					if ( c != ' ' )
						stacks.get(st).add(c);
				}
			}
			if ( line < 11 )
				continue;

			Scanner sc = new Scanner(a);
			int n = sc.nextInt();
			LinkedList<Character> f = stacks.get(sc.nextInt());
			LinkedList<Character> t = stacks.get(sc.nextInt());
			for (int i = n - 1; i >= 0; i--) {
				//t.push(f.poll());
				t.push(f.remove(i));
			}
		}
		String f1 = "";
		for (int i = 1; i <= 9; i++) {
			f1 += stacks.get(i).peek();
		}
		System.out.println(f1);
	}
}
