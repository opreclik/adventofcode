package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Day10Computer {

	static long sum = 0;

	static long cycle = 0;
	static long x = 1;

	static void tick() {
		long mod = cycle++ % 40;
		if ( cycle < 221 ) {
			if ( mod == 19 )
				sum += cycle * x;
		}

		System.out.print(Math.abs(mod - x) <= 1 ? '#' : ' ');
		if ( mod == 39 )
			System.out.println("");
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "10.txt";

		for (String a : Files.readAllLines(Paths.get(in))) {
			Scanner sc = new Scanner(a);
			String cmd = sc.next();
			tick();
			if ( cmd.equals("addx") ) {
				tick();
				x += sc.nextInt();
			}
		}
		System.out.println(sum);
	}
}
