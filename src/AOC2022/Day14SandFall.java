package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import utils.Utils;

public class Day14SandFall {

	static int fall(int[][] ar, int my) {
		for (int sand = 1; true; sand++) {
			int x = 500;
			int y = 0;
			while (y < my) {
				y++;
				if ( ar[y][x] == 0 ) {
					continue;
				}
				if ( ar[y][x - 1] == 0 ) {
					x--;
					continue;
				}
				if ( ar[y][x + 1] == 0 ) {
					x++;
					continue;
				}
				ar[--y][x] = 2;
				break;
			}

			if ( y >= my )
				return sand - 1;

			if ( ar[0][500] == 2 )
				return sand;
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "14.txt";

		int[][] ar = new int[200][1000];
		int my = 0;
		for (String a : Files.readAllLines(Paths.get(in))) {
			String[] coor = a.split("->");
			int ox = -1;
			int oy = -1;
			for (String s : coor) {
				Scanner sc = new Scanner(s.trim());
				sc.useDelimiter(",");
				int x = sc.nextInt();
				int y = sc.nextInt();
				if ( ox != -1 ) {
					if ( x == ox ) {
						long dy = Utils.sigi(y - oy);
						for (int i = oy; i != y; i += dy) {
							ar[i][x] = 1;
						}
						ar[y][x] = 1;
					}
					if ( y == oy ) {
						long dx = Utils.sigi(x - ox);
						for (int i = ox; i != x; i += dx) {
							ar[y][i] = 1;
						}
						ar[y][x] = 1;
					}
				}
				ox = x;
				oy = y;
				my = Math.max(my, y);
			}
		}

		//my += 2;
		for (int i = 1; i != 999; i += 1) {
			//ar[my][i] = 1;
		}

		System.out.println(fall(ar, my));
	}
}
