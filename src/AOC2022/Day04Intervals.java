package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Day04Intervals {

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		for (String a : Files.readAllLines(Paths.get("04.txt"))) {
			Scanner sc = new Scanner(a.replaceAll("[,-]", " "));
			int j = sc.nextInt();
			int k = sc.nextInt();
			int l = sc.nextInt();
			int m = sc.nextInt();
			if ( in(j, k, l, m) || in(l, m, j, k) )
				sum1++;
			if ( in(j, k, m, l) )
				sum2++;
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}

	static boolean in(int j, int k, int l, int m) {
		return j <= l && k >= m;
	}
}
