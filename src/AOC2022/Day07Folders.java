package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Day07Folders {

	static class Dir {

		Dir parent;
		String name;
		List<Dir> dirs = new ArrayList<>();
		long files;
		long total;

		Dir(String name, Dir p) {
			this.name = name;
			this.parent = p;
		}

		boolean isEmpty() {
			return dirs.isEmpty() && files == 0;
		}

		Dir find(String name) {
			return dirs.stream().filter(d -> d.name.equals(name)).findFirst().get();
		}

		long count() {
			for (Dir dir : dirs)
				total += dir.count();
			total += files;
			return total;
		}
	}

	static long sum = 0;
	static Dir min = null;

	static void rec(Dir d, long need) {
		if ( d.total <= 100000 ) {
			sum += d.total;
		}
		if ( d.total >= need ) {
			if ( min == null || min.total > d.total ) {
				min = d;
			}
		}
		for (Dir dir : d.dirs)
			rec(dir, need);
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "07.txt";
		Dir root = new Dir("", null);
		root.parent = root;
		Dir act = root;
		boolean skip = false;
		for (String a : Files.readAllLines(Paths.get(in))) {
			Scanner sc = new Scanner(a);
			if ( a.startsWith("$") ) {
				skip = false;

				sc.next();
				String cmd = sc.next();
				if ( cmd.equals("cd") ) {
					String name = sc.next();
					if ( name.equals("/") ) {
						act = root;
						continue;
					}
					if ( name.equals("..") ) {
						act = act.parent;
						continue;
					}
					act = act.find(name);
					continue;
				}
				if ( cmd.equals("ls") ) {
					if ( !act.isEmpty() )
						skip = true;
					continue;
				}
				continue;
			}
			if ( skip )
				continue;
			if ( a.startsWith("dir") ) {
				sc.next();
				act.dirs.add(new Dir(sc.next(), act));
				continue;
			}
			act.files += sc.nextLong();
		}
		root.count();

		long need = 30000000 - (70000000 - root.total);
		rec(root, need);
		System.out.println(sum);
		System.out.println(min.total);
	}
}
