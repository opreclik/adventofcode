package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day06Repeat {

	public static void main(String[] args) throws IOException {
		for (String a : Files.readAllLines(Paths.get("06.txt"))) {
			for (int i = 13; i < a.length(); i++) {
				boolean r[] = new boolean[200];
				boolean done = true;
				for (int j = i - 13; j <= i; j++) {
					char c = a.charAt(j);
					if ( r[c] ) {
						done = false;
						break;
					}
					r[c] = true;
				}
				if ( done ) {
					System.out.println(i + 1);
					break;
				}
			}
		}
	}
}
