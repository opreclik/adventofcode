package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import utils.ParseNum;
import utils.Utils;

public class Day22MapCube {

	static List<String> map = new ArrayList<>();

	static ParseNum parse = new ParseNum();

	static class Pos {

		int x;
		int y;
		int d;

		int nx;
		int ny;
		int nd;

		Pos() {
			String s = map.get(0);
			while (s.charAt(x) != '.')
				x++;
		}

		boolean in() {
			if ( ny < 0 || ny >= map.size() || nx < 0 )
				return false;
			String s = map.get(ny);
			if ( nx >= s.length() )
				return false;
			return s.charAt(nx) != ' ';
		}

		long go(String s) {
			for (int i = 0; i < s.length(); i++) {
				i = parse.read(s, i);
				for (int j = 0; j < parse.val; j++) {
					nx = x + Utils.dx[d];
					ny = y + Utils.dy[d];
					if ( !in() ) {
						//wrap();
						wrap2(nx, ny);
					}
					if ( map.get(ny).charAt(nx) == '#' )
						break;
					x = nx;
					y = ny;
					d = nd;
				}
				if ( i == s.length() )
					break;
				nd = d = switch (s.charAt(i)) {
					case 'R' -> d == 3 ? 0 : d + 1;
					case 'L' -> d == 0 ? 3 : d - 1;
					default -> throw new RuntimeException("failed dir");
				};
			}
			return 1000 * (y + 1) + 4 * (x + 1) + d;
		}

		void wrap() {
			int dir = (d + 2) % 4;
			nx = x;
			ny = y;
			while (in()) {
				nx += Utils.dx[dir];
				ny += Utils.dy[dir];
			}
			nx += Utils.dx[d];
			ny += Utils.dy[d];
		}

		void set(int ix, int iy, int id) {
			nx = ix;
			ny = iy;
			nd = id;
		}

		void wrap2(int ax, int ay) {
			if ( ay == -1 && ax < 100 && ax >= 50 && d == 3 )  // 1 to 12
				set(0, ax + 100, 0);
			else if ( ay == -1 && ax >= 100 && ax < 150 && d == 3 )  // 2 to 14
				set(ax - 100, 199, 3);
			else if ( ay < 50 && ax == 49 && d == 2 ) // 3 to 9
				set(0, 149 - ay, 0);
			else if ( ay < 50 && ax == 150 && d == 0 ) // 4 to 10
				set(99, 149 - ay, 2);
			else if ( ay == 50 && ax >= 100 && ax < 150 && d == 1 ) // 5 to 7
				set(99, ax - 50, 2);
			else if ( ay >= 50 && ay < 100 && ax == 49 && d == 2 ) // 6 to 8
				set(ay - 50, 100, 1);
			else if ( ay >= 50 && ay < 100 && ax == 100 && d == 0 ) // 7 to 5
				set(ay + 50, 49, 3);
			else if ( ay == 99 && ax < 50 && d == 3 ) // 8 to 6
				set(50, ax + 50, 0);
			else if ( ay >= 100 && ay < 150 && ax == -1 && d == 2 ) // 9 to 3
				set(50, 149 - ay, 0);
			else if ( ay >= 100 && ay < 150 && ax == 100 && d == 0 ) // 10 to 4
				set(149, 149 - ay, 2);
			else if ( ay == 150 && ax >= 50 && ax < 100 && d == 1 ) // 11 to 13
				set(49, ax + 100, 2);
			else if ( ay >= 150 && ay < 200 && ax == -1 && d == 2 ) // 12 to 1
				set(ay - 100, 0, 1);
			else if ( ay >= 150 && ay < 200 && ax == 50 && d == 0 ) // 13 to 11
				set(ay - 100, 149, 3);
			else if ( ay == 200 && ax < 50 && d == 1 ) // 14 to 2
				set(ax + 100, 0, 1);
			if ( ax == nx && ay == ny && d == nd )
				System.out.println("fail " + ax + " " + ay + " " + d);
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "22.txt";
		map = Files.readAllLines(Paths.get(in));
		String cmd = map.remove(map.size() - 1);
		System.out.println(new Pos().go(cmd));
	}
}
