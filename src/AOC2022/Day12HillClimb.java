package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import utils.Utils;

public class Day12HillClimb {

	static class Coord {

		int x;
		int y;
		int m;

		Coord(int x, int y, int m) {
			this.x = x;
			this.y = y;
			this.m = m;
		}

		public int hashCode() {
			return x + 1001 * y;
		}

		public boolean equals(Object obj) {
			if ( this == obj )
				return true;
			if ( obj instanceof Coord c )
				return c.x == x && c.y == y;
			return false;
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "12.txt";

		int[][] m = new int[170][50];

		int row = 1;
		int col = 1;
		int endx = 0, endy = 0;
		Set<Coord> w = new HashSet<>();

		for (String a : Files.readAllLines(Paths.get(in))) {
			for (byte b : a.getBytes()) {
				if ( b == 'S' || b == 'a' ) {
					w.add(new Coord(col, row, 'a'));
					b = 0;
				}
				if ( b == 'E' ) {
					endx = col;
					endy = row;
					b = 'z';
				}
				m[col++][row] = b;
			}
			row++;
			col = 1;
		}

		for (int it = -1; !w.isEmpty(); it--) {
			Set<Coord> n = new HashSet<>();
			for (Coord c : w) {
				for (int i = 0; i < 4; i++) {
					int x = c.x + Utils.dx[i];
					int y = c.y + Utils.dy[i];
					if ( m[x][y] > 0 && m[x][y] - c.m <= 1 ) {
						n.add(new Coord(x, y, m[x][y]));
						m[x][y] = it;
					}
				}
			}
			w = n;
		}

		System.out.println(-m[endx][endy]);
	}
}
