package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Day02RockPaperSciss {

	static int diff(int a, int b) {
		int ret = b - a;
		return switch (ret) {
			case -2 -> 1;
			case 2 -> -1;
			default -> ret;
		};
	}

	static int score(int a, int b) {
		return 3 + 3 * diff(a, b) + b + 2;
	}

	public static void main(String[] args) throws IOException {
		long sum1 = 0;
		long sum2 = 0;
		for (String a : Files.readAllLines(Paths.get("02.txt"))) {
			int x = a.charAt(0) - 'B';
			int y = a.charAt(2) - 'Y';
			sum1 += score(x, y);
			sum2 += score(x, diff(-x, y));
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
