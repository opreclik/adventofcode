package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import utils.Utils;

public class Day08TreeHouse {

	static boolean test(int x, int y, List<String> trees, int[] c, int dir, int col) {
		int v = trees.get(x).charAt(y);
		while (true) {
			x += Utils.dx[dir];
			y += Utils.dy[dir];
			if ( x < 0 || y < 0 || x == trees.size() || y == col )
				return true;
			c[dir]++;
			if ( trees.get(x).charAt(y) >= v )
				return false;
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "08.txt";
		List<String> trees = Files.readAllLines(Paths.get(in));
		int col = trees.get(0).length();
		long sum1 = 2 * (col + trees.size()) - 4;
		long sum2 = 0;
		for (int i = 1; i < trees.size() - 1; i++) {
			for (int j = 1; j < col - 1; j++) {
				int c[] = new int[4];
				boolean vis = false;
				for (int d = 0; d < 4; d++) {
					if ( test(i, j, trees, c, d, col) )
						vis = true;
				}
				if ( vis )
					sum1++;
				int s = c[0] * c[1] * c[2] * c[3];
				sum2 = Math.max(s, sum2);
			}
		}
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
