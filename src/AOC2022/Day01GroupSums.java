package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Day01GroupSums {

	public static void main(String[] args) throws IOException {
		PriorityQueue<Long> pri = new PriorityQueue<>(Comparator.reverseOrder());
		long sum = 0;
		for (String a : Files.readAllLines(Paths.get("01.txt"))) {
			if ( a.isEmpty() ) {
				pri.add(sum);
				sum = 0;
				continue;
			}
			sum += Long.parseLong(a);
		}
		System.out.println(pri.peek());
		System.out.println(pri.poll() + pri.poll() + pri.poll());
	}
}
