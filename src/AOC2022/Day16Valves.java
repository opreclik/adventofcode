package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day16Valves {

	static List<Valve> plus = new ArrayList<>();

	static class Valve {

		String name;
		int rate;
		int idx = 0;
		int bit = 0;
		List<String> next = new ArrayList<>();
		List<Valve> valves = new ArrayList<>();

		int dist;
		int[] d = new int[20];

		Valve(String name, int rate) {
			this.name = name;
			this.rate = rate;
		}

		boolean isReady(int mask) {
			return (bit & mask) > 0;
		}

		int open(int mask) {
			return mask & ~bit;
		}

		void fillDist() {
			dist = 0;
			List<Valve> strm = new ArrayList<>();
			strm.add(this);
			for (int i = 0; i < strm.size(); i++) {
				Valve v = strm.get(i);
				for (Valve n : v.valves) {
					if ( n.dist == -1 ) {
						n.dist = v.dist + 1;
						strm.add(n);
					}
				}
			}
		}
	}

	static class Pos {

		Valve v1;
		Valve v2;
		int mask;
		int tot;

		Pos(Valve v1, Valve v2, int mask, int tot) {
			this.v1 = v1;
			this.v2 = v2;
			this.mask = mask;
			this.tot = tot;
		}

		static int k(int i1, int i2, int m) {
			int a = Math.min(i1, i2);
			int b = Math.max(i1, i2);
			return a << 23 | b << 17 | m;
		}

		int key() {
			return k(v1.idx, v2.idx, mask);
		}
	}

	static long cnt = 0;

	static Map<Integer, Pos> cache = new HashMap<>();

	static class Min {

		Map<Integer, Pos> map = new HashMap<>();
		int min;
		int max;

		Min(int min) {
			this.min = min;
		}

		long update(Pos p, Valve v1, boolean op1, Valve v2, boolean op2) {
			int m = 30 - min;
			int tot = p.tot;
			int mask = p.mask;
			if ( op1 ) {
				tot += m * v1.rate;
				mask = v1.open(mask);
			}
			if ( op2 ) {
				tot += m * v2.rate;
				mask = v2.open(mask);
			}
			max = Math.max(max, tot);
			if ( mask == 0 ) {
				return tot;
			}

			int key = Pos.k(v1.idx, v2.idx, mask);
			Pos exist = cache.get(key);
			if ( exist != null ) {
				if ( exist.tot >= tot ) {
					return exist.tot;
				}
				exist.tot = tot;
				map.put(key, exist);
				return tot;
			}
			Pos pos = new Pos(v1, v2, mask, tot);
			cache.put(key, pos);
			map.put(key, pos);
			cnt++;
			return tot;
		}
	}

	static int forward1(Pos init) {
		Min[] mins = new Min[31];
		for (int i = 0; i < mins.length; i++) {
			mins[i] = new Min(i);
		}
		mins[0].map.put(init.key(), init);
		int ret = 0;
		for (int i = 0; i < 30; i++) {
			for (Pos p : mins[i].map.values()) {
				if ( p.v1.isReady(p.mask) ) {
					mins[i + 1].update(p, p.v1, true, p.v1, false);
				}
				for (int n = 0; n < plus.size(); n++) {
					Valve nv = plus.get(n);
					if ( !nv.isReady(p.mask) )
						continue;
					int t = i + p.v1.d[n] + 1;
					if ( t > 30 )
						continue;
					mins[t].update(p, nv, true, nv, false);
				}
			}
			ret = Math.max(mins[i + 1].max, ret);
		}
		return ret;
	}

	static int forward2(Pos init) {
		int ret = 0;

		Min act = new Min(4);
		act.map.put(init.key(), init);
		for (int i = 4; i < 30; i++) {
			Min next = new Min(i + 1);
			for (Pos p : act.map.values()) {
				if ( p.v1.isReady(p.mask) ) {
					if ( p.v2 != p.v1 && p.v2.isReady(p.mask) ) {
						next.update(p, p.v1, true, p.v2, true);
					}
					for (Valve v2 : p.v2.valves) {
						next.update(p, p.v1, true, v2, false);
					}
				}
				if ( p.v2.isReady(p.mask) ) {
					for (Valve v1 : p.v1.valves) {
						next.update(p, v1, false, p.v2, true);
					}
				}
				for (Valve v1 : p.v1.valves) {
					for (Valve v2 : p.v2.valves) {
						next.update(p, v1, false, v2, false);
					}
				}
			}
			act = next;
			ret = Math.max(act.max, ret);
			System.out.println(i + " " + cnt + " " + ret);
		}
		return ret;
	}

	static int rec = 0;
	static long it = 0;

	static void recursive1(Valve v, int min, int mask, int score) {
		it++;
		if ( score > rec ) {
			System.out.println("max score: " + score + " it: " + it);
			rec = score;
		}
		for (int m = mask, i = 0; m > 0; m >>= 1, i++) {
			if ( (m & 1) == 0 )
				continue;
			int t = min + v.d[i] + 1;
			if ( t >= 30 ) {
				continue;
			}
			Valve n = plus.get(i);
			recursive1(n, t, n.open(mask), score + (30 - t) * n.rate);
		}
	}

	static void recursive2(Valve v, int min, Valve w, int delay, int mask, int score) {
		it++;
		if ( score > rec ) {
			System.out.println("max score: " + score + " it: " + it);
			rec = score;
		}
		if ( mask == 0 )
			return;
		recursive1(v, min, mask, score);
		recursive1(w, min + delay, mask, score);
		if ( delay > 0 ) {
			for (int m = mask, i = 0; m > 0; m >>= 1, i++) {
				if ( (m & 1) == 0 )
					continue;
				int nt = v.d[i] + 1;
				int t1 = min + nt;
				if ( t1 >= 30 ) {
					continue;
				}
				Valve n = plus.get(i);
				Valve f1 = nt > delay ? w : n;
				Valve f2 = nt > delay ? n : w;
				recursive2(f1, Math.min(t1, min + delay), f2, Math.abs(nt - delay), n.open(mask), score + (30 - t1) * n.rate);
			}
			return;
		}
		for (int m = mask, i = 0; m > 0; m >>= 1, i++) {
			if ( (m & 1) == 0 )
				continue;
			int t1 = min + v.d[i] + 1;
			if ( t1 >= 30 ) {
				continue;
			}
			Valve n1 = plus.get(i);
			int sc = score + (30 - t1) * n1.rate;
			int mask2 = n1.open(mask);
			for (int m2 = mask2, j = 0; m2 > 0; m2 >>= 1, j++) {
				if ( (m2 & 1) == 0 )
					continue;
				int t2 = min + w.d[j] + 1;
				if ( t2 >= 30 ) {
					continue;
				}
				Valve n2 = plus.get(j);
				Valve f1 = t2 < t1 ? n2 : n1;
				Valve f2 = t2 < t1 ? n1 : n2;
				recursive2(f1, Math.min(t1, t2), f2, Math.abs(t1 - t2), n2.open(mask2), sc + (30 - t2) * n2.rate);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "16.txt";

		Map<String, Valve> m = new HashMap<>();
		int bit = 1;
		int idx = 1;
		for (String a : Files.readAllLines(Paths.get(in))) {
			a = a.replaceAll("Valve", "")
				.replace("has flow rate=", "")
				.replaceAll(",", "");
			String[] sp = a.split(";");
			Scanner sc = new Scanner(sp[0].trim());
			Valve v = new Valve(sc.next(), sc.nextInt());
			sc = new Scanner(sp[1]);
			while (sc.hasNext()) {
				String n = sc.next().trim();
				if ( n.length() != 2 || n.equals("to") )
					continue;
				v.next.add(n);
			}
			m.put(v.name, v);
			v.idx = idx++;
			if ( v.rate > 0 ) {
				v.bit = bit;
				bit *= 2;
				plus.add(v);
			}
		}
		for (Valve v : m.values()) {
			for (String n : v.next) {
				v.valves.add(m.get(n));
			}
		}
		plus.sort(Comparator.<Valve>comparingInt(v -> v.rate).reversed());
		bit = 1;
		for (Valve v : plus) {
			v.bit = bit;
			bit *= 2;
		}
		for (Valve v : m.values()) {
			for (Valve n : m.values()) {
				n.dist = -1;
			}
			v.fillDist();
			for (int i = 0; i < plus.size(); i++) {
				v.d[i] = plus.get(i).dist;
			}
		}

		Valve first = m.get("AA");

		recursive1(first, 0, bit - 1, 0);
		recursive2(first, 4, first, 0, bit - 1, 0);
		System.out.println("it: " + it);

		Pos init = new Pos(first, first, bit - 1, 0);
		System.out.println(forward1(init));
		System.out.println(forward2(init));
	}
}
