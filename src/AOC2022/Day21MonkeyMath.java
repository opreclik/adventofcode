package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class Day21MonkeyMath {

	static Map<String, Monkey> m = new HashMap<>();

	static class Monkey {

		String name;
		long val;
		String m1;
		String m2;
		String op;

		Monkey mon1;
		Monkey mon2;

		Monkey hmn;
		long have;

		Monkey(String name, long val) {
			this.name = name;
			this.val = val;
		}

		Monkey(String name, String m1, String m2, String op) {
			this.name = name;
			this.m1 = m1;
			this.m2 = m2;
			this.op = op;
		}

		static Monkey fromSplit(String[] s) {
			return s.length == 2 ? new Monkey(s[0], Long.parseLong(s[1])) : new Monkey(s[0], s[1], s[3], s[2]);
		}

		long eval() {
			if ( op == null )
				return val;
			mon1 = m.get(m1);
			mon2 = m.get(m2);
			long a = mon1.eval();
			long b = mon2.eval();
			//System.out.println(a + " " + b);
			val = switch (op) {
				case "+" -> a + b;
				case "-" -> a - b;
				case "*" -> a * b;
				case "/" -> a / b;
				default -> throw new RuntimeException("unk op " + op);
			};
			return val;
		}

		boolean isHuman() {
			if ( name.equals("humn") )
				return true;
			if ( op == null )
				return false;
			if ( mon1.isHuman() ) {
				hmn = mon1;
				have = mon2.val;
				return true;
			}
			if ( mon2.isHuman() ) {
				hmn = mon2;
				have = mon1.val;
				return true;
			}
			return false;
		}

		long calc(long v) {
			long need = switch (op) {
				case "+" -> v - have;
				case "-" -> (mon1 == hmn) ? v + have : have - v;
				case "*" -> v / have;
				case "/" -> (mon1 == hmn) ? have * v : have / v;
				default -> throw new RuntimeException("unk op " + op);
			};
			return hmn.op == null ? need : hmn.calc(need);
		}
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "21.txt";

		Monkey root = null;
		for (String a : Files.readAllLines(Paths.get(in))) {
			String[] sp = a.replaceAll(":", "").split(" ");
			Monkey mon = Monkey.fromSplit(sp);
			m.put(mon.name, mon);
			if ( mon.name.equals("root") )
				root = mon;
		}

		System.out.println(root.eval());

		root.isHuman();
		System.out.println(root.hmn.calc(root.have));
	}
}
