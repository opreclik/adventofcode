package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day18Cubes {

	static Map<Integer, int[]> s = new HashMap<>();
	static int[] acc = new int[]{0, 0, 0};

	static void copy(int[] c) {
		System.arraycopy(c, 0, acc, 0, 3);
	}

	static int key(int[] c) {
		return c[0] << 12 | c[1] << 6 | c[2];
	}

	static boolean isFree() {
		return !s.containsKey(key(acc));
	}

	static class Face {

		int[] c;
		int dir;
		int inc;

		Face(int[] coord, int dir, int inc) {
			c = coord;
			this.dir = dir;
			this.inc = inc;
		}

		boolean match(int id, int diff) {
			return id == dir && inc == diff;
		}

		boolean touch(Face f) {
			int sum = 0;
			int[] diff = new int[3];
			for (int i = 0; i < 3; i++) {
				diff[i] = f.c[i] - c[i];
				int a = Math.abs(diff[i]);
				if ( a > 1 )
					return false;
				sum += a;
			}
			if ( sum > 2 )
				return false;
			if ( sum == 1 )
				return match(f.dir, f.inc);
			if ( sum == 0 ) {
				copy(c);
				acc[dir] += inc;
				acc[f.dir] += f.inc;
				return isFree();
			}
			for (int i = 0; i < 3; i++) {
				if ( diff[i] != 0 )
					continue;

				int a = i == 0 ? 2 : i - 1;
				int b = i == 2 ? 0 : i + 1;
				return match(b, diff[b]) && f.match(a, -diff[a])
					   || match(a, diff[a]) && f.match(b, -diff[b]);
			}
			return false;
		}
	}

	static class Comp {

		List<Face> faces = new ArrayList<>();

		Comp(Face f) {
			faces.add(f);
		}

		boolean touch(Face f) {
			return faces.stream().anyMatch(face -> face.touch(f));
		}

		boolean touch(Comp c) {
			return c.faces.stream().anyMatch(this::touch);
		}
	}

	static int faces = 0;

	static void addFace(List<Comp> c, int[] coord, int dir, int diff) {
		copy(coord);
		acc[dir] += diff;
		if ( !isFree() )
			return;
		faces++;
		Face f = new Face(coord, dir, diff);
		for (Comp comp : c) {
			if ( comp.touch(f) ) {
				comp.faces.add(f);
				return;
			}
		}
		c.add(new Comp(f));
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "18.txt";

		for (String a : Files.readAllLines(Paths.get(in))) {
			Scanner sc = new Scanner(a.replaceAll(",", " "));
			int[] c = new int[]{sc.nextInt(), sc.nextInt(), sc.nextInt()};
			s.put(key(c), c);
		}
		List<Comp> c = new ArrayList<>();
		for (int[] coord : s.values()) {
			for (int i = 0; i < 3; i++) {
				addFace(c, coord, i, 1);
				addFace(c, coord, i, -1);
			}
		}
		long sum2 = 0;
		while (true) {
			int size = c.size();
			for (int i = 0; i < c.size(); i++) {
				Comp c1 = c.get(i);
				sum2 = Math.max(c1.faces.size(), sum2);
				for (int j = i + 1; j < c.size(); j++) {
					Comp c2 = c.get(j);
					if ( c1.touch(c2) ) {
						c1.faces.addAll(c2.faces);
						c.remove(j--);
					}
				}
			}
			if ( size == c.size() )
				break;
		}
		System.out.println(faces);
		System.out.println(sum2);
	}
}
