package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import utils.ParseNum;
import utils.Utils;

public class Day13PacketsCompare {

	static ParseNum num = new ParseNum();

	static int parse(String str, int i, List<Object> packet) {
		while (true) {
			char c = str.charAt(i);
			if ( c == ']' )
				return i + 1;
			if ( c == ',' ) {
				i++;
				continue;
			}
			if ( Utils.isInt(c) ) {
				i = num.read(str, i);
				packet.add(num.val);
			}
			if ( c == '[' ) {
				List<Object> sub = new ArrayList<>();
				i = parse(str, i + 1, sub);
				packet.add(sub);
			}
		}
	}

	static List<Object> parse(String str) {
		List<Object> p = new ArrayList<>();
		parse(str, 1, p);
		return p;
	}

	public static void main(String[] args) throws IOException {
		int ex = -1;
		String in = ex > 0 ? "ex.txt" : "13.txt";

		long idx = 1;
		long sum1 = 0;
		List<Object> div2 = parse("[[2]]");
		List<Object> div6 = parse("[[6]]");
		List<List<Object>> ar = new ArrayList<>();
		ar.add(div2);
		ar.add(div6);
		List<String> lines = Files.readAllLines(Paths.get(in));
		for (int i = 0; i < lines.size(); i++) {
			List<Object> a = parse(lines.get(i));
			List<Object> b = parse(lines.get(i + 1));
			if ( cmp(a, b) < 1 )
				sum1 += idx;

			ar.add(a);
			ar.add(b);
			i += 2;
			idx++;
		}

		System.out.println(sum1);

		ar.sort(Day13PacketsCompare::cmp);
		int idx2 = ar.indexOf(div2) + 1;
		int idx6 = ar.indexOf(div6) + 1;
		System.out.println(idx2 * idx6);
	}

	static List<Object> getList(Object o) {
		if ( o instanceof Integer ) {
			List<Object> ar = new ArrayList<>();
			ar.add(o);
			return ar;
		}
		return (List<Object>)o;
	}

	static int cmp(List<Object> a, List<Object> b) {
		int na = a.size();
		int nb = b.size();
		int min = Math.min(na, nb);
		for (int i = 0; i < min; i++) {
			Object oa = a.get(i);
			Object ob = b.get(i);
			if ( oa instanceof Integer va) {
				if ( ob instanceof Integer vb ) {
					if ( va < vb )
						return -1;
					if ( va > vb )
						return 1;
					continue;
				}
			}

			int r = cmp(getList(oa), getList(ob));
			if ( r != 0 )
				return r;
		}
		if ( na < nb )
			return -1;
		if ( na > nb )
			return 1;
		return 0;
	}
}
