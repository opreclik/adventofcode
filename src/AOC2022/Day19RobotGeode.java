package AOC2022;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Day19RobotGeode {

// ore clay obsidian geode
	static class State {

		int[] res = new int[4];
		int[] rob = new int[4];
		int[] cost = new int[4];
		int[] cost2 = new int[4];
		int m;

		int it = 0;

		State() {
			rob[0] = 1;
		}

		State(State s) {
			cost = s.cost;
			cost2 = s.cost2;
			m = s.m;
		}

		boolean can(int i) {
			if ( cost[i] > res[0] )
				return false;
			if ( i == 0 )
				return rob[0] < m;
			if ( i == 1 )
				return rob[1] < cost2[2];
			return cost2[i] <= res[i - 1];
		}

		int can3() {
			int add = cost[3] <= res[0] && cost2[3] <= res[2] ? 1 : 0;
			return res[3] + (rob[3] << 1) + add;
		}

		void robot(State s, int i) {
			System.arraycopy(s.res, 0, res, 0, 4);
			System.arraycopy(s.rob, 0, rob, 0, 4);
			res[0] -= cost[i];
			if ( i > 0 )
				res[i - 1] -= cost2[i];
			for (int j = 0; j < res.length; j++)
				res[j] += rob[j];
			rob[i]++;
		}

		void step(State s) {
			int i = s.it;
			while (i >= 0 && !s.can(i))
				i--;

			it = 3;
			for (int j = 0; j < res.length; j++) {
				rob[j] = s.rob[j];
				res[j] = rob[j] + s.res[j];
			}
			if ( i >= 0 ) {
				res[0] -= cost[i];
				if ( i > 0 )
					res[i - 1] -= cost2[i];
				rob[i]++;
			}
			s.it = i - 1;
		}
	}

	static class Val {

		int[] res = new int[4];
		int[] rob = new int[4];

		Val(State s) {
			copy(s);
		}

		int cmp(State s) {
			boolean hi = false;
			boolean lo = false;
			for (int i = 0; i < res.length; i++) {
				if ( res[i] < s.res[i] )
					hi = true;
				if ( res[i] > s.res[i] )
					lo = true;
				if ( rob[i] < s.rob[i] )
					hi = true;
				if ( rob[i] > s.rob[i] )
					lo = true;
				if ( hi && lo )
					return 2;
			}
			if ( hi )
				return 1;
			if ( lo )
				return -1;
			return 0;
		}

		void copy(State s) {
			System.arraycopy(s.res, 0, res, 0, 4);
			System.arraycopy(s.rob, 0, rob, 0, 4);
		}
	}

	static class Cache {

		LinkedList<Val> list = new LinkedList<>();

		boolean test(State s) {
			ListIterator<Val> it = list.listIterator();
			boolean replaced = false;
			while (it.hasNext()) {
				Val v = it.next();
				int c = v.cmp(s);
				if ( c < 1 ) {
					return false;
				}
				if ( c == 2 )
					continue;
				if ( replaced ) {
					it.remove();
					continue;
				}
				replaced = true;
				v.copy(s);
			}
			if ( !replaced ) {
				list.add(new Val(s));
			}
			return true;
		}
	}

	static long solveIter(int id, State init) {
		State[] min = new State[32];
		Cache[] cache = new Cache[32];
		for (int i = 0; i < min.length; i++) {
			min[i] = new State(init);
			cache[i] = new Cache();
		}
		min[0] = init;

		long max = 0;
		init.it = 3;
		for (int m = 0; m >= 0;) {
			State s = min[m];
			if ( s.it == -2 ) {
				m--;
				continue;
			}
			if ( m == 30 ) {
				int geo = s.can3();
				if ( geo > max ) {
					max = geo;
					System.out.println("temp " + id + " res " + max);
				}
				m = 29;
				continue;
			}
			State n = min[m + 1];
			n.step(s);
			if ( m >= 23 || cache[m].test(n) )
				m++;
		}
		System.out.println("finished " + id + " res " + max);
		return max;
	}

	static long sum1 = 0;
	static long sum2 = 1;

	static void acc(int id, long s) {
		sum1 += id * s;
		sum2 *= s;
	}

	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		int ex = -1;
		boolean sm = false;
		String in = ex > 0 ? "ex.txt" : "19.txt";

		List<CompletableFuture<Void>> futs = new ArrayList<>();
		for (String a : Files.readAllLines(Paths.get(in))) {
			String[] sp = a.replaceAll("[:.]", "").split(" ");
			int id = Integer.parseInt(sp[1]);
			if ( !sm && id > 3 )
				continue;
			State s = new State();
			s.cost[0] = Integer.parseInt(sp[6]);
			s.cost[1] = Integer.parseInt(sp[12]);
			s.cost[2] = Integer.parseInt(sp[18]);
			s.cost2[2] = Integer.parseInt(sp[21]);
			s.cost[3] = Integer.parseInt(sp[27]);
			s.cost2[3] = Integer.parseInt(sp[30]);
			s.m = Math.max(s.cost[1], Math.max(s.cost[2], s.cost[3]));
			//acc(id, solveIter(id, s));

			futs.add(CompletableFuture
				.supplyAsync(() -> solveIter(id, s))
				.thenAccept(m -> acc(id, m))
			);
		}

		CompletableFuture.allOf(futs.toArray(new CompletableFuture[futs.size()])).join();
		System.out.println(sum1);
		System.out.println(sum2);
	}
}
